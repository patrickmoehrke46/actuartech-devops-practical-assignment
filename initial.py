# Initiate the dash class, creating app and server objects
# Also add meta_tags to make dashboard mobile friendly
import os
import dash
import dash_bootstrap_components as dbc
# Import link to icons and set format
FONT_AWESOME = "https://use.fontawesome.com/releases/v5.10.2/css/all.css"
external_stylesheets=[dbc.themes.BOOTSTRAP, FONT_AWESOME]

dashboard = dash.Dash(__name__, suppress_callback_exceptions=True,
                external_stylesheets=external_stylesheets,
                meta_tags=[{'name': 'viewport',
                            'content': 'width=device-width, initial-scale=1.0'}]
                )

server = dashboard.server