FROM python:3.8-buster
RUN apt update && apt upgrade -y
WORKDIR dashboard/
COPY . .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python3", "index.py"]
