import pandas as pd
import numpy as np

# Import Covid data
covid = pd.read_csv("https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/owid-covid-data.csv")

# Data Management ======================================================================================================
covid.replace(np.nan, 0, inplace=True)
covid.date = pd.to_datetime(covid.date)
covid.continent = pd.Categorical(covid.continent)
covid.location = pd.Categorical(covid.location)

# Create lists for dropdown --------------------------------------------------------------------------------------------
regions = np.unique(list(covid.location))
continents = np.unique(list(covid.continent))
features = list(covid)[4:42]

# Find dataset first and last date -------------------------------------------------------------------------------------
min_date = np.min(covid.date)
max_date = np.max(covid.date)
