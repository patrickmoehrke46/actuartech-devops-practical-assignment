from dash import html

layout = html.Div([
    html.H1('Further References'),
    html.P('1.',
           style={'display': 'inline-block', "margin-left":"25px"}
           ),
    html.P('Hosting and deployment:',
           style={'display': 'inline-block', "margin-left":"10px"}
           ),
    html.A("info@actuartech.com",
           href='info@actuartech.com',
           target="_blank",
           style={'display': 'inline-block',"margin-left":"5px"}
           ),
    html.P(''),
    html.P('2.',
           style={'display': 'inline-block', "margin-left":"25px"}
           ),
    html.P('Learn Dash:',
           style={'display': 'inline-block', "margin-left":"10px"}
           ),
    html.A("https://dash.plotly.com",
           href='https://dash.plotly.com',
           target="_blank",
           style={'display': 'inline-block',"margin-left":"5px"}
           ),
    html.P(''),
    html.P('3.',
           style={'display': 'inline-block', "margin-left":"25px"}
           ),
    html.P('Open source & professional software for data science teams, May 2021:',
           style={'display': 'inline-block', "margin-left":"10px"}
           ),
    html.A("info@actuartech.com",
           href='info@actuartech.com',
           target="_blank",
           style={'display': 'inline-block',"margin-left":"5px"}
           ),
    html.P(''),
    html.P('4.',
           style={'display': 'inline-block', "margin-left":"25px"}
           ),
    html.P('Diana Beltekian Hannah Ritchie, Esteban Ortiz-Ospina. Coronavirus pandemic (covid-19).' +
           'Our World in Data, 2020:',
           style={'display': 'inline-block', "margin-left":"10px"}
           ),
    html.A("https://ourworldindata.org/coronavirus/",
           href='https://ourworldindata.org/coronavirus/',
           target="_blank",
           style={'display': 'inline-block', "margin-left":"5px"}
           ),
    html.P(''),
    html.P('5.',
           style={'display': 'inline-block', "margin-left":"25px"}
           ),
    html.P('Dash Enterprise App Gallery:',
           style={'display': 'inline-block', "margin-left":"10px"}
           ),
    html.A("https://dash.gallery/Portal/",
           href='https://dash.gallery/Portal/',
           target="_blank",
           style={'display': 'inline-block', "margin-left":"5px"}
           )
    ]
)
