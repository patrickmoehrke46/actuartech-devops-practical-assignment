from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output
from covid_data import covid, regions, continents, features, min_date, max_date
from initial import dashboard


layout = html.Div([
    # Region 1 and 2
    dbc.Row([
        dbc.Col(html.H4('Select Region 1')),
        dbc.Col(html.H4('Select Region 2'))
    ]),
    dbc.Row([
        dbc.Col(dcc.Dropdown(
            id='region1-dropdown-graph-page2',
            options=[{'label': region, 'value': region} for region in regions],
            multi=False,
            value='South Africa'
        )),
        dbc.Col(dcc.Dropdown(
            id='region2-dropdown-graph-page2',
            options=[{'label': region, 'value': region} for region in regions],
            multi=False,
            value='United Kingdom'
        ))
    ]),
    # Space
    html.P(''),
    # Feature and moving average
    dbc.Row([
        dbc.Col(html.H4('Select Feature to Plot')),
        dbc.Col(html.H4('Enter Moving Average (MA)'))
    ]),
    dbc.Row([
        dbc.Col(dcc.Dropdown(
            id='feature-dropdown-graph-page2',
            options=[{'label': feature, 'value': feature} for feature in features],
            multi=False,
            value='new_cases',
            # style={'display': 'inline-block'}
        )),
        dbc.Col(dcc.Input(
           id='moving-average-graph-page2',
           type="number",
           value=7,
           min=1,
           max=1000000,
           step=1,
           style={'height': '35px'}
        ))
    ]),
    # Space
    html.P(''),
    # Date range
    html.H4('Select Date Range'),
    dcc.DatePickerRange(
        id='date-picker-range-graph-page2',
        min_date_allowed=min_date,
        max_date_allowed=max_date,
        initial_visible_month=max_date,
        start_date=min_date,
        end_date=max_date,
        style={'height': '20px'}
    ),
    # Space
    html.P('', style={'height': '40px'}),
    # Graph
    dcc.Graph(
        id="graph2-graphs-page2",
        config={"displayModeBar": False},
    )
])

@dashboard.callback(Output('graph2-graphs-page2', 'figure'),
              [Input('region1-dropdown-graph-page2', 'value'),
               Input('region2-dropdown-graph-page2', 'value'),
               Input('date-picker-range-graph-page2', 'start_date'),
               Input('date-picker-range-graph-page2', 'end_date'),
               Input('feature-dropdown-graph-page2', 'value'),
               Input('moving-average-graph-page2', 'value')])

def update_graph2(region1, region2, start_date, end_date, feature, MA):
    # Creating strings for naming the columns/series
    r1_F = str(region1 + " " + feature)
    r2_F = str(region2 + " " + feature)
    r1_MA = str(region1 + " " + feature + ".MA" + str(MA))
    r2_MA = str(region2 + " " + feature + ".MA" + str(MA))

    # Subset the dataframes, creating series
    # Subset by selected region and date
    covid_updatedR1 = covid[(covid["location"] == region1) & (covid["date"] >= start_date) & (covid["date"] <= end_date)]
    covid_updatedR2 = covid[(covid["location"] == region2) & (covid["date"] >= start_date) & (covid["date"] <= end_date)]
    # Subset by selected feature and date and rename feature column
    covid_updatedR1 = covid_updatedR1[['date', feature]]
    covid_updatedR1.rename(columns={feature: r1_F}, inplace=True)
    covid_updatedR2 = covid_updatedR2[['date', feature]]
    covid_updatedR2.rename(columns={feature: r2_F}, inplace=True)
    # Create moving averages
    covid_updatedR1_MA = covid_updatedR1.rolling(window=int(MA)).mean()  # .replace(np.nan, 0, inplace=True)
    covid_updatedR2_MA = covid_updatedR2.rolling(window=int(MA)).mean()  # .replace(np.nan, 0, inplace=True)
    # Combine dataframes of feature and moving averages
    covid_updatedR1.insert(2, r1_MA, covid_updatedR1_MA)
    covid_updatedR2.insert(2, r2_MA, covid_updatedR2_MA)
    # Plot figure
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=covid_updatedR1.date, y=covid_updatedR1[r1_F],
                             mode = 'lines', name = r1_F,
                             line=dict(color= 'firebrick', width=2)
                             )
                  )
    fig.add_trace(go.Scatter(x=covid_updatedR2.date, y=covid_updatedR2[r2_F],
                             mode='lines', name=r2_F,
                             line=dict(color='royalblue', width=2)
                             )
                  )
    fig.add_trace(go.Scatter(x=covid_updatedR1.date, y=covid_updatedR1[r1_MA],
                             mode='lines', name=r1_MA,
                             line=dict(color='firebrick', width=2, dash='dash')
                             )
                  )
    fig.add_trace(go.Scatter(x=covid_updatedR1.date, y=covid_updatedR2[r2_MA],
                             mode='lines', name=r2_MA,
                             line = dict(color='royalblue', width=2,dash='dash')
                             )
                  )
    fig.update_layout(title={'text':str("Plot of " + feature + " from " + str(start_date) + " to " + str(end_date)),
                             'y':0.9,
                             'x':0.5,
                             'xanchor': 'center',
                             'yanchor': 'top'}
                      )
    fig.update_xaxes(title_text='Date')
    fig.update_yaxes(title_text=feature)

    return fig
