from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
from covid_data import covid, regions, min_date, max_date
from initial import dashboard

card_icon = {
    "color": "white",
    "textAlign": "center",
    "fontSize": 30,
    "margin": "auto",
}
# Set layout
layout = html.Div([
    # Dropdown selection for region-------------------------------------------------------------------------------------
    dbc.Row([
        dbc.Col(html.H4('Select Region')),
        dbc.Col(html.H4('Select Date'))
    ]),
    dbc.Row([
        dbc.Col(dcc.Dropdown(
            id='region-dropdown-overview-page',
            options=[{'label': region, 'value': region} for region in regions],
            multi=False,
            value='South Africa',
            #style={'height': '45px'}
        )),
        dbc.Col(dcc.DatePickerSingle(
            id='my-date-picker-single',
            min_date_allowed=min_date,
            max_date_allowed=max_date,
            initial_visible_month=max_date,
            date=max_date,
        ))
    ]),
    # Space
    html.P('', style={"margin-top": "75px"}),
    # Cards with info ==================================================================================================
    # Row 1: Daily
    dbc.Row([
        # Card 1: Daily cases ------------------------------------------------------------------------------------------
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-user-plus", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="danger"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('DAILY CASES', className='card-title'),
                    html.P(id='daily-cases', className='card-text')
                ]),
                color="#ff0000",
                inverse=True
            )
        ])
        ),
        # Card 2: Daily deaths
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-bed", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="warning"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('DAILY DEATHS', className='card-title'),
                    html.P(id='daily-deaths', className='card-text')
                ]),
                color="darkorange",
                inverse=True
            )
        ])),
        # Card 3: Daily Vaccines
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-syringe", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="primary"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('DAILY VACCINES', className='card-title'),
                    html.P(id='daily-vaccines', className='card-text')
                ]),
                color="dodgerblue ",
                inverse=True
            )
        ])),
    ]),

    # Space
    html.P('', style={"margin-top": "25px"}),

    # Row 2: Total
    dbc.Row([
        # Card 4: Total cases
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-user-plus", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="info"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('TOTAL CASES', className='card-title'),
                    html.P(id='total-cases', className='card-text')
                ]),
                color="deepskyblue",
                inverse=True
            )
        ])
        ),
        # Card 5: Total deaths
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-bed", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="success"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('TOTAL DEATHS', className='card-title'),
                    html.P(id='total-deaths', className='card-text')
                ]),
                color="#008000",
                inverse=True
            )
        ])),
        # Card 6: Total vaccines
        dbc.Col(dbc.CardGroup([
            dbc.Card(
                html.Div(className="fas fa-syringe", style=card_icon),
                className="bg-primary",
                style={"maxWidth": 75},
                color="dark"
            ),
            dbc.Card(
                dbc.CardBody([
                    html.H5('TOTAL VACCINES', className='card-title'),
                    html.P(id='total-vaccines', className='card-text')
                ]),
                color="secondary",
                inverse=True
            )
        ])),
    ])
])

@dashboard.callback(
    [Output(component_id='daily-cases', component_property='children'),
     Output(component_id='daily-deaths', component_property='children'),
     Output(component_id='daily-vaccines', component_property='children'),
     Output(component_id='total-cases', component_property='children'),
     Output(component_id='total-deaths', component_property='children'),
     Output(component_id='total-vaccines', component_property='children')],
    [Input(component_id='my-date-picker-single', component_property='date'),
     Input(component_id='region-dropdown-overview-page', component_property='value')]
)

def card_values(date, region):
    # Card update function - daily cases
    daily_cases = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "new_cases"])
    daily_deaths = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "new_deaths"])
    daily_vaccines = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "new_vaccinations"])
    total_cases = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "total_cases"])
    total_deaths = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "total_deaths"])
    total_vaccines = int(covid.loc[(covid["location"] == region) & (covid["date"] == date), "total_vaccinations"])

    return  daily_cases, daily_deaths, daily_vaccines, total_cases, total_deaths, total_vaccines

