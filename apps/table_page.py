from dash import dash_table
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
from covid_data import covid, regions, min_date, max_date
from initial import dashboard

layout = html.Div([
    dbc.Row([
        dbc.Col(html.H4('Select Region')),
        dbc.Col(html.H4('Select Date'))
    ]),
    dbc.Row([
        dbc.Col(dcc.Dropdown(
            id='region-dropdown-table-page',
            options=[{'label': region, 'value': region} for region in regions],
            multi=False,
            value='South Africa',
        )),
        dbc.Col(dcc.DatePickerRange(
            id='date-picker-range-table-page',
            min_date_allowed=min_date,
            max_date_allowed=max_date,
            initial_visible_month=max_date,
            start_date=min_date,
            end_date=max_date,
        ))
    ]),

    html.P('', style={"margin-top": "75px"}),
    dash_table.DataTable(
        id='datatable-interactivity',
        columns=[
            {"name": i, "id": i, "deletable": False, "selectable": False} for i in covid.columns
        ],
        editable=False,
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        column_selectable=False,
        row_selectable=False,
        row_deletable=False,
        selected_columns=[],
        selected_rows=[],
        page_action="native",
        page_current=0,
        page_size=25,
        style_table={'height': '300px', 'overflowY': 'auto'},
        css=[{'selector': 'table', 'rule': 'table-layout: fixed'}],
        style_cell={'width': '200px', 'minWidth': '200px', 'maxWidth': '200px',
                    'textOverflow': 'ellipsis',
                    'overflow': 'hidden'},
    ),
])

@dashboard.callback(Output('datatable-interactivity', 'data'),
              [Input('region-dropdown-table-page', 'value'),
               Input('date-picker-range-table-page', 'start_date'),
               Input('date-picker-range-table-page', 'end_date')])
def update_table(value, start_date, end_date):
    return covid.loc[(covid["location"] == value) & (covid["date"] >= start_date) & (covid["date"] <= end_date)].to_dict("records")
